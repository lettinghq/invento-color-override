var colorOverride = angular.module('invento-color-override', []);

colorOverride.factory('InventoColorOverride', function($localStorage, $timeout) {
    var ColorOverride = function() {};

    ColorOverride.prototype = {
        // External values
        colors : function() {
            return this.__colors;
        },

        parse : function() {
            var newColor;

            for (var color = 0; color < arguments.length; color++) {
                newColor = this.__variations(arguments[color]);

                if (newColor) {
                    this.__colors.push(newColor);
                }
            }
        },

        stylesheet : function(styles) {
            var sheet       = [],
                nodeList    = document.getElementsByTagName('style'),
                element     = document.createElement('style');
                styles      = this.__toStylesheet(styles);

            if (styles) {
                element.type        = 'text/css';
                element.innerHTML   = styles;
                document.getElementsByTagName('head')[0].appendChild(element);
            }
        },

        // Internal values
        __colors : [],

        __trueHex : function(hex) {
            hex = this.__validHex(hex);

            if (!hex) {
                return false;
            }

            hex[0] = hex[0].length === 1 ? '' + hex[0] + hex[0] : '' + hex[0];
            hex[1] = hex[1].length === 1 ? '' + hex[1] + hex[1] : '' + hex[1];
            hex[2] = hex[2].length === 1 ? '' + hex[2] + hex[2] : '' + hex[2];

            return hex[0] + hex[1] + hex[2];
        },

        __hexToRgb : function(hex) {
            hex = this.__validHex(hex);
            
            return hex ? [parseInt(hex[1], 16), parseInt(hex[2], 16), parseInt(hex[3], 16)] : false;
        },

        __componentToHex : function(component) {
            var hex = component.toString(16);

            return hex.length === 1 ? '0' + hex.toUpperCase() : hex.toUpperCase();
        },

        __rgbToHex : function(r, g, b) {
            return '#' + this.__componentToHex(r) + this.__componentToHex(g) + this.__componentToHex(b);
        },

        __saturate : function(hex, sat) {
            var color = this.__hexToRgb(hex),
                sat   = sat / 100,
                gray  = color[0] * 0.3086 + color[1] * 0.6094 + color[2] * 0.820;

            color[0] = Math.round(color[0] * sat + gray * (1 - sat));
            color[1] = Math.round(color[1] * sat + gray * (1 - sat));
            color[2] = Math.round(color[2] * sat + gray * (1 - sat));

            return this.__rgbToHex(color[0], color[1], color[2]);
        },

        __validHex : function(hex) {
            return /^#?([a-f\d]{1,2})([a-f\d]{1,2})([a-f\d]{1,2})$/i.exec(hex);
        },

        __luminance : function(hex, luminance) {
            var newColor = '#', colorPart = false;

            if (hex) {
                for (var i = 0; i < 3; i++) {
                    colorPart   = parseInt(hex.substr(i * 2, 2), 16);
                    colorPart   = Math.round(Math.min(Math.max(0, colorPart + (colorPart * luminance)), 255)).toString(16);
                    newColor   += ('00' + colorPart).substr(colorPart.length);
                }

                return newColor.toUpperCase();
            }

            return false;
        },

        __variations : function(hex) {
            var colors = {};

            hex = this.__trueHex(hex);

            if (!hex) return null;

            colors.vdark   = this.__luminance(hex, -0.4);
            colors.dark    = this.__luminance(hex, -0.2);
            colors.default = this.__luminance(hex, 0);
            colors.light   = this.__luminance(hex, 0.2);
            colors.vlight  = this.__luminance(hex, 0.4);

            colors.pastel = {
                vdark   : this.__saturate(colors.vdark, 50),
                dark    : this.__saturate(colors.dark, 50),
                default : this.__saturate(colors.default, 50),
                light   : this.__saturate(colors.light, 50),
                vlight  : this.__saturate(colors.vlight, 50)
            };

            colors.text = {
                vdark   : this.__contrast(colors.vdark),
                dark    : this.__contrast(colors.dark),
                default : this.__contrast(colors.default),
                light   : this.__contrast(colors.light),
                vlight  : this.__contrast(colors.vlight)
            };

            return colors;
        },

        __contrast : function(hex) {
            var dec = parseInt(hex.replace(/#/g, ''), 16),
                lum = (0.2126 * ((dec >> 16) & 0xff)) + (0.2126 * ((dec >> 8)  & 0xff)) + (0.2126 * ((dec >> 0)  & 0xff));

            return lum < 140 ? '#F6F6F6' : '#3A3A3A';
        },

        __toCss : function(className, css) {
            if (typeof className !== 'string' || typeof css !== 'object') return false;

            var flattened = className + '{';

            for (var key in css) {
                flattened += key + ':' + css[key] + '!important;';
            }

            return flattened.substring(0, flattened.length -1) + '}';
        },

        __toStylesheet : function(styles) {
            if (typeof styles !== 'object') return false;

            var stylesheet = '';

            for (var attribute in styles) {
                if (typeof styles[attribute].className !== 'undefined' && typeof styles[attribute].css !== 'undefined') {
                    stylesheet += this.__toCss(styles[attribute].className, styles[attribute].css);
                }
            }

            return stylesheet;
        }
    };
    return {
        NewInstance : function() {
            var instance = new ColorOverride();

            instance.parse.apply(instance, arguments);

            return instance;
        }
    };
});
