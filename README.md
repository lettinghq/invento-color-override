* Include the following lines at the bottom of your index.html:

```
#!html

<script src="bower_components/invento-color-override/dist/js/color-override.js"></script>
```

* Include the following package in your app's module:

```
#!javascript

'invento-color-override'
```